export const getAssessments = (students) => {
  const assessmentNames = [];
  const questionCounts = [];
  students.forEach((student) => {
    const keys = Object.keys(student);
    const grades = keys.filter((key) => key.includes('Grade on'));
    grades.forEach((grade) => {
      const name = grade.substring(9);
      if (!assessmentNames.includes(name)) {
        assessmentNames.push(name);
        questionCounts.push(
          keys.filter((key) => key.includes(name) && key.includes('(Possible)'))
            .length
        );
      }
    });
  });

  const assessments = assessmentNames.map((name, idx) => {
    return { name: name, questions: questionCounts[idx] };
  });
  return assessments;
};
