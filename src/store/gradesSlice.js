import { createSlice } from '@reduxjs/toolkit';
import { getAssessments } from './helpers';

const initialState = {
  data: [],
  assessments: [],
};

const gradesSlice = createSlice({
  name: 'grades',
  initialState,
  reducers: {
    processData: (state, action) => {
      const assessments = getAssessments(action.payload);
      Object.assign(state.data, action.payload);
      Object.assign(state.assessments, assessments);
    },
  },
});

export const { processData } = gradesSlice.actions;
export default gradesSlice.reducer;
