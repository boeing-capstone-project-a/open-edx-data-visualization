import { configureStore } from '@reduxjs/toolkit';
import gradesReducer from './gradesSlice';

export const store = configureStore({
  reducer: {
    grades: gradesReducer,
  },
});
