import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';
import { CssBaseline } from '@mui/material';
import Home from './components/pages/Home';
import Assessments from './components/pages/Assessments';
import Navbar from './components/Navbar';
import { processData } from './store/gradesSlice';

const INTERVAL = 1000 * 60 * 1; // 1 minute

function App() {
  const dispatch = useDispatch();

  const fetchData = async () => {
    const response = await fetch('http://localhost:12345/csvToPy.json');
    if (response.ok) {
      const json = await response.json();
      dispatch(processData(json));
    } else {
      console.error('Unable to fetch data');
    }
  };

  fetchData();

  useEffect(() => {
    const periodicDataRefresh = globalThis?.setInterval(() => {
      fetchData();
    }, INTERVAL);
    return () => {
      periodicDataRefresh && globalThis?.clearInterval(periodicDataRefresh);
    };
  });

  return (
    <BrowserRouter>
      <CssBaseline />
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/assessments" element={<Assessments />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
