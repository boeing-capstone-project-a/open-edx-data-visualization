import { AppBar, Toolbar, Typography } from '@mui/material';
import { Link } from 'react-router-dom';
import '../css/NavbarStyles.css';

function Navbar() {
  return (
    <div>
      <AppBar>
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Open edX Data Visualization
          </Typography>
          <div>
            <Link to="/" className="buttonLink">
              Home
            </Link>
            <Link to="/assessments" className="buttonLink">
              Assessments
            </Link>
          </div>
        </Toolbar>
      </AppBar>
      <Toolbar id="anchor" />
    </div>
  );
}

export default Navbar;
