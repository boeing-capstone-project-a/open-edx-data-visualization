import { Box, Typography } from '@mui/material';

function CourseTitle() {
  return (
    <Box padding="25px">
      <Typography variant="h2">
        Calhoun Transdisciplinary Fusion Studio 1
      </Typography>
    </Box>
  );
}
export default CourseTitle;
