import { useSelector } from 'react-redux';
import { Grid, Paper, Typography } from '@mui/material';

function BasicStatistics() {
  const data = useSelector((state) => state.grades.data);
  const assessments = useSelector((state) => state.grades.assessments);

  const totalEnrolled = data.length;

  const totalPassing = data.reduce((passingCount, datum) => {
    const grade = datum['Overall Grade'] * 100;
    if (grade >= 60) {
      passingCount++;
    }
    return passingCount;
  }, 0);
  const percentagePassing = ((totalPassing / data.length) * 100).toFixed(1);

  //for percentage active users I'm seeing how many students have attempted all the quizzes
  //we can change this later!
  const totalActive = data.reduce((activeCount, datum) => {
    const unattemptedAssessments = assessments.filter(
      (assessment) => datum[`Grade on ${assessment.name}`] === 'Not Attempted'
    );
    if (unattemptedAssessments.length === 0) {
      activeCount++;
    }
    return activeCount;
  }, 0);
  const percentageActive = ((totalActive / data.length) * 100).toFixed(1);

  let stats = [
    {
      id: 1,
      name: 'Total Enrollment',
      value: totalEnrolled,
    },
    {
      id: 2,
      name: 'Percentage Passing',
      value: percentagePassing + '%',
    },
    {
      id: 3,
      name: 'Percentage Active Users',
      value: percentageActive + '%',
    },
  ];

  return (
    <Grid sx={{ flexGrow: 1, padding: 40 + 'px' }} container spacing={2}>
      <Grid item xs={12}>
        <Grid container justifyContent="center" spacing={5}>
          {stats.map((stat) => (
            <Grid key={stat.id} item>
              <Paper
                sx={{
                  height: 140,
                  width: 200,
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <Typography variant="h3">{stat.value}</Typography>
                {stat.name}
              </Paper>
            </Grid>
          ))}
        </Grid>
      </Grid>
    </Grid>
  );
}

export default BasicStatistics;
