import { Chart, registerables } from 'chart.js';
import { Bar } from 'react-chartjs-2';

Chart.register(...registerables);

function AssessmentDistributionChart({ data }) {
  return (
    <div
      className="chart-container"
      style={{ width: 40 + 'vw', padding: 50 + 'px' }}
    >
      <h2 style={{ textAlign: 'center' }}>{data.datasets[0].label}</h2>
      <Bar
        data={data}
        options={{
          scales: {
            y: {
              title: {
                display: true,
                text: 'Count',
              },
              beginAtZero: true,
            },
            x: {
              title: {
                display: true,
                text: 'Grade',
              },
            },
          },
          plugins: {
            legend: { display: true },
          },
        }}
      />
    </div>
  );
}

export default AssessmentDistributionChart;
