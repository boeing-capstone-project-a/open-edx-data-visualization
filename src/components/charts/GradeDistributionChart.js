import { useSelector } from 'react-redux';
import { Chart, registerables } from 'chart.js';
import { Bar } from 'react-chartjs-2';

Chart.register(...registerables);

function GradeDistributionChart() {
  const data = useSelector((state) => state.grades.data);
  if (data.length === 0) return;

  const chartOptions = {
    scales: {
      y: {
        title: {
          display: true,
          text: 'Count',
        },
        beginAtZero: true,
      },
      x: {
        title: {
          display: true,
          text: 'Grade',
        },
      },
    },
    plugins: {
      legend: { display: true },
      tooltip: {
        callbacks: {
          footer: (context) => {
            return gradeUsernames[context[0].label].join('\n');
          },
        },
      },
    },
  };

  const gradeCounts = data.reduce(
    (counts, d) => {
      const grade = d['Overall Grade'] * 100;
      if (grade >= 90) {
        counts.A++;
      } else if (grade >= 80) {
        counts.B++;
      } else if (grade >= 70) {
        counts.C++;
      } else if (grade >= 60) {
        counts.D++;
      } else {
        counts.F++;
      }
      return counts;
    },
    { A: 0, B: 0, C: 0, D: 0, F: 0 }
  );

  const gradeUsernames = data.reduce(
    (usernames, d) => {
      const grade = d['Overall Grade'] * 100;
      if (grade >= 90) {
        usernames.A.push(d.Username);
      } else if (grade >= 80) {
        usernames.B.push(d.Username);
      } else if (grade >= 70) {
        usernames.C.push(d.Username);
      } else if (grade >= 60) {
        usernames.D.push(d.Username);
      } else {
        usernames.F.push(d.Username);
      }
      return usernames;
    },
    { A: [], B: [], C: [], D: [], F: [] }
  );

  const overallData = {
    labels: ['A', 'B', 'C', 'D', 'F'],
    datasets: [
      {
        label: 'Overall Grade Distribution',
        data: [
          gradeCounts.A,
          gradeCounts.B,
          gradeCounts.C,
          gradeCounts.D,
          gradeCounts.F,
        ],
        backgroundColor: '#ADD8E6',
      },
    ],
  };

  return (
    <div
      className="chart-container"
      style={{ width: 50 + 'vw', padding: 50 + 'px' }}
    >
      <h2 style={{ textAlign: 'center' }}>Grade Distribution</h2>
      <Bar data={overallData} options={chartOptions} />
    </div>
  );
}

export default GradeDistributionChart;
