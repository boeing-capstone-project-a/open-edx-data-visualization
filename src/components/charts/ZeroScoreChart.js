import { useSelector } from 'react-redux';
import { Chart, registerables } from 'chart.js';
import { Pie } from 'react-chartjs-2';
import { Typography } from '@mui/material';

Chart.register(...registerables);

function ZeroScoreChart() {
  const data = useSelector((state) => state.grades.data);
  const assessments = useSelector((state) => state.grades.assessments);

  if (data.length === 0) return;

  const chartData = assessments.map((assessment) => {
    const questions = Array.from(
      { length: assessment.questions },
      (_, i) => `Q${i + 1}`
    );
    const dataByQuestion = questions.map((question) => {
      const missed = data.filter(
        (d) =>
          d[`${assessment.name} - ${question} (Earned)`] === '0' ||
          d[`${assessment.name} - ${question} (Earned)`] === '0.0'
      );
      const students = missed.map((user) => {
        return user.Username;
      });
      return {
        question: question,
        count: missed.length,
        missedStudents: students,
      };
    });

    return {
      labels: dataByQuestion.map((d) => d.question),
      datasets: [
        {
          label: `0 Scores in ${assessment.name}`,
          data: dataByQuestion.map((d) => d.count),
          backgroundColor: [
            '#FF6384',
            '#36A2EB',
            '#FFCE56',
            '#4BC0C0',
            '#9966FF',
            '#FF8F00',
            '#2E8B57',
          ],
        },
      ],
      missedStudents: dataByQuestion.map((d) => d.missedStudents),
    };
  });

  return (
    <div style={{ textAlign: 'center' }}>
      <Typography variant="h3" sx={{ marginTop: '25px' }}>
        Most Missed Questions in Each Assessment
      </Typography>
      <div
        className="chart-container"
        style={{ display: 'flex', justifyContent: 'center', padding: '50px' }}
      >
        {chartData.map((data, index) => (
          <div key={index} style={{ width: '30vw', marginRight: '20px' }}>
            <h2 style={{ textAlign: 'center' }}>{assessments[index].name}</h2>
            <Pie
              data={data}
              options={{
                plugins: {
                  title: {
                    display: true,
                    text: `Most Missed Questions in ${assessments[index].name}`,
                  },
                  legend: { display: true },
                  tooltip: {
                    callbacks: {
                      footer: (context) => {
                        let missed = '';
                        data.missedStudents[context[0].dataIndex].map(
                          (student) => {
                            missed += student;
                            missed += '\n';
                            return missed;
                          }
                        );
                        return '\nStudents: \n' + missed;
                      },
                    },
                  },
                },
              }}
            />
          </div>
        ))}
      </div>
    </div>
  );
}

export default ZeroScoreChart;
