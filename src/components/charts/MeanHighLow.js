import { useSelector } from 'react-redux';
import { Chart, registerables } from 'chart.js';
import { Line } from 'react-chartjs-2';
import { Typography } from '@mui/material';

Chart.register(...registerables);

function GradeLineChart() {
  const data = useSelector((state) => state.grades.data);
  const assessments = useSelector((state) => state.grades.assessments);
  if (data.length === 0) return;

  // Calculating mean, high score, and low score for each quiz
  const overallGradeData = data.map(
    (datum) => parseFloat(datum['Overall Grade']) * 100
  );
  const overallGradeMean =
    overallGradeData.reduce((acc, val) => acc + val) / overallGradeData.length;
  const overallGradeMax = Math.max(...overallGradeData);
  const overallGradeMin = Math.min(...overallGradeData);

  const assessmentsGradeData = assessments.map((assessment) => {
    const assessmentData = data.map(
      (datum) => parseFloat(datum[`Grade on ${assessment.name}`]) * 100
    );
    const assessmentGradeMean =
      assessmentData.reduce((acc, val) => acc + val) / assessmentData.length;
    const assessmentGradeMax = Math.max(...assessmentData);
    const assessmentGradeMin = Math.min(...assessmentData);

    return {
      data: assessmentData,
      mean: assessmentGradeMean,
      max: assessmentGradeMax,
      min: assessmentGradeMin,
    };
  });

  const assessmentNames = assessments.map((assessment) => assessment.name);
  const gradeMeans = assessmentsGradeData.map((datum) => datum.mean);
  const gradeMaxes = assessmentsGradeData.map((datum) => datum.max);
  const gradeMins = assessmentsGradeData.map((datum) => datum.min);

  const chartData = {
    labels: ['Overall', ...assessmentNames],
    datasets: [
      {
        label: 'Mean Grade',
        data: [overallGradeMean, ...gradeMeans],
        borderColor: '#3CB371',
        fill: false,
      },
      {
        label: 'High Score',
        data: [overallGradeMax, ...gradeMaxes],
        borderColor: '#ADD8E6',
        fill: false,
      },
      {
        label: 'Low Score',
        data: [overallGradeMin, ...gradeMins],
        borderColor: '#FFA07A',
        fill: false,
      },
    ],
  };

  const chartOptions = {
    scales: {
      y: {
        title: {
          display: true,
          text: 'Grade',
        },
        beginAtZero: true,
      },
      x: {
        title: {
          display: true,
          text: 'Assessment',
        },
      },
    },
  };

  return (
    <div
      className="chart-container"
      style={{ width: 50 + 'vw', padding: 50 + 'px' }}
    >
      <Typography variant="h3" sx={{ textAlign: 'center', padding: 20 + 'px' }}>
        Mean, High, Low Score of Each Assessment
      </Typography>
      <Line data={chartData} options={chartOptions} />
    </div>
  );
}

export default GradeLineChart;
