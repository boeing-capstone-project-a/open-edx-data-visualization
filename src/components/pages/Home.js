import CourseTitle from '../CourseTitle';
import BasicStatistics from '../BasicStatistics';
import GradeDistributionChart from '../charts/GradeDistributionChart';

import { Box } from '@mui/material';

function Home() {
  return (
    <Box
      display="flex"
      justifyContent="center"
      alignItems="center"
      flexDirection="column"
    >
      <CourseTitle />
      <BasicStatistics />
      <GradeDistributionChart />
    </Box>
  );
}

export default Home;
