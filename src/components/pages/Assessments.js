import { useSelector } from 'react-redux';
import { Grid, Typography } from '@mui/material';
import AssessmentDistributionChart from '../charts/AssessmentDistributionChart';
import ZeroScoreChart from '../charts/ZeroScoreChart';
import LineGraph from '../charts/MeanHighLow';
import StudentDistributionChart from '../charts/StudentDistributionChart';

function getGradeCounts(data, assessmentName) {
  return data.reduce(
    (counts, d) => {
      const grade = d[assessmentName] * 100;
      if (grade >= 90) {
        counts[0]++;
      } else if (grade >= 80) {
        counts[1]++;
      } else if (grade >= 70) {
        counts[2]++;
      } else if (grade >= 60) {
        counts[3]++;
      } else {
        counts[4]++;
      }
      return counts;
    },
    [0, 0, 0, 0, 0]
  );
}

function getAllStudentGrades(data, assessmentName, bgColor) {
  return data.reduce(
    (chartData, d) => {
      chartData.labels.push(d['Username']);
      chartData.datasets[0].data.push(d[assessmentName] * 100);
      return chartData;
    },
    {
      labels: [],
      datasets: [{ label: assessmentName, data: [], backgroundColor: bgColor }],
    }
  );
}

function Assessments() {
  const data = useSelector((state) => state.grades.data);
  const assessments = useSelector((state) => state.grades.assessments);
  if (data.length === 0) return;

  const gradeLabels = ['A', 'B', 'C', 'D', 'F'];
  const bgColors = ['#3CB371', '#FFC0CB', '#FFD700'];

  const chartData = assessments.map((assessment, idx) => {
    return {
      labels: gradeLabels,
      datasets: [
        {
          label: `Grade on ${assessment.name}`,
          data: getGradeCounts(data, `Grade on ${assessment.name}`),
          backgroundColor: bgColors[idx % 3],
        },
      ],
    };
  });

  return (
    <div>
      <div>
        <Typography
          variant="h2"
          sx={{ textAlign: 'center', padding: 20 + 'px' }}
        >
          Grade Distribution by Assessment
        </Typography>
        <Grid container justifyContent="center">
          {chartData.map((datum, idx) => (
            <Grid container item xs={6} justifyContent="center" key={idx}>
              <AssessmentDistributionChart data={datum} />
            </Grid>
          ))}
        </Grid>
      </div>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <ZeroScoreChart />
        <LineGraph />
      </div>
      <div>
        <Typography
          variant="h3"
          sx={{ textAlign: 'center', padding: 20 + 'px' }}
        >
          Assessment Distribution by Student
        </Typography>
        <Grid container justifyContent="center">
          {assessments.map((assessment, idx) => (
            <Grid container item xs={6} justifyContent="center" key={idx}>
              <StudentDistributionChart
                data={getAllStudentGrades(
                  data,
                  `Grade on ${assessment.name}`,
                  bgColors[idx % 3]
                )}
              />
            </Grid>
          ))}
        </Grid>
      </div>
    </div>
  );
}

export default Assessments;
