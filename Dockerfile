# prepare
FROM node:18-alpine as builder
WORKDIR /app
COPY . .

# build
RUN npm ci 
RUN npm run build

# run
FROM nginx:1.24.0-alpine as production
COPY --from=builder /app/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]
