# Open Edx Data Visualization

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Using node and npm: https://docs.npmjs.com/downloading-and-installing-node-js-and-npm#using-a-node-version-manager-to-install-nodejs-and-npm \
React tutorial: https://reactjs.org/tutorial/tutorial.html \
ChartJS and React tutorial: https://blog.logrocket.com/using-chart-js-react/

## Running the Project Locally

### Running with npm start

To run the project locally, you may need to install dependencies first:\
`npm install`

After which, you can start the application:\
`npm start`

The application should be available at http://localhost:3000. \
The page will reload when you make changes.\
You may also see any lint errors in the console.\
`CTRL+C` to quit.

### Serving a production build

If you do not have serve installed, you can install it with the following command:\
`npm install -g serve`

To build the project:\
`npm run build`

To serve it locally:\
`serve -s build`

The previous command serves to port 3000 by default. To specify a port:\
`serve -s build -l 3001`

The application should be available at http://localhost:3000. \
`CTRL+C` to quit.

### Running with docker

To build the container for the first time:\
`docker compose up --build`

This may take a few minutes. When the build has completed, the application should be available at http://localhost:8080. \
`CTRL+C` to quit.

After the container has been built, it can be restarted with the following command:\
`docker compose up`
